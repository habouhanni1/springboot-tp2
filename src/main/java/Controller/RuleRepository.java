package Controller;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface RuleRepository  extends CrudRepository<Rule, Long> {
}
