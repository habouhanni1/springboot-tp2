package Controller;

import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class ProductController {

    @GetMapping("/products/{id}")
    public String getProduct(@PathVariable("id") String qr )throws UnirestException {
        return  new ProductService().getProductSummary(qr);
    }

    @GetMapping("/products/score/{id}")
    public String getScore(@PathVariable("id") String qr )throws UnirestException {
        return  new ProductService().getScore(qr);
    }

}
