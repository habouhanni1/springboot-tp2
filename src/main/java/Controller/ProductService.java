package Controller;

import Utils.Constants;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;

public class ProductService {
    private final static String SEPARATOR = ", ";
    private final static String BR = "<br/>";
    private String details;
    private String score;

    private JSONObject getProductByQR(final String barCode) throws UnirestException {
        JSONObject jsonResponse
                = Unirest.get("https://fr.openfoodfacts.org/api/v0/produit/" + barCode + ".json")
                .header("accept", "application/json").queryString("apiKey", "123")
                .asJson()
                .getBody()
                .getObject();
        return jsonResponse;
    }

    public String getProductSummary(final String barCode) throws UnirestException {
        final JSONObject jsonResponse = getProductByQR(barCode);
        if (jsonResponse.getInt("status") != 1) {
            return "product Not Found !";
        } else {
            JSONObject nutrimentsNode = jsonResponse.getJSONObject("product").getJSONObject("nutriments");
            constuctSummary(nutrimentsNode);
            return details;
        }
    }

    public String getScore(final String barCode) throws UnirestException {
        final JSONObject jsonResponse = getProductByQR(barCode);
        if (jsonResponse.getInt("status") != 1) {
            return "product Not Found !";
        } else {
            JSONObject nutrimentsNode = jsonResponse.getJSONObject("product").getJSONObject("nutriments");
            constuctSummary(nutrimentsNode);
            return score;
        }
    }

    private void constuctSummary(final JSONObject nutrimentsNode) throws UnirestException {

        final StringBuilder pros = new StringBuilder("Qualité : ");
        final StringBuilder cons = new StringBuilder("Défaults : ");
        int nutritionScore = 0;

        for (String element : Constants.getNegatives()) {
            final int points = ScoringService.getPoints(element, nutrimentsNode.getDouble(element));
            if (points <= 3) {
                pros.append(element).append(SEPARATOR);
                nutritionScore += points;
            } else if (points >= 7) {
                cons.append(element).append(SEPARATOR);
                nutritionScore -= points;
            }
        }

        for (String element : Constants.getPositives()) {
            final int points = ScoringService.getPoints(element, nutrimentsNode.getDouble(element));
            if (points >= 2) {
                pros.append(element).append(SEPARATOR);
                nutritionScore += points;
            } else if (points <= 0) {
                cons.append(element).append(SEPARATOR);
                nutritionScore -= points;
            }
        }

        //delete last "," from cons !
        String classe = new StringBuilder(" - Votre classe est : ").append(ScoringService.getClasse(nutritionScore)).toString();
        score =new StringBuilder("Score nutritionnel : ").append(nutritionScore).append(classe).toString();
        details =pros.append(cons).toString();

    }



}
