package Controller;

import org.springframework.data.repository.CrudRepository;

public interface ScoreNutritionRepository extends CrudRepository<ScoreNutrition, Long> {
}
