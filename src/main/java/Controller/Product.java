package Controller;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

@Entity
public class Product {
    @Id
    private int id;
    private String barCode;
    private String content;
    private int energy;
    private int saturated_fat;
    private double sugars;
    private int salt_100g;
    private double proteins;
    private double fiber;
    @ElementCollection
    private List<String> qualities;
    @ElementCollection
    private List<String> defaults;
    private double score;


    public Product(String codebar, int energy, int saturated_fat, double sugars, int salt_100g, double proteins, double fiber) {
        this.energy = energy;
        this.saturated_fat = saturated_fat;
        this.sugars = sugars;
        this.salt_100g = salt_100g;
        this.proteins = proteins;
        this.fiber = fiber;
        this.barCode = codebar;
    }

    public Product() {
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getEnergy() {
        return energy;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public int getSaturated_fat() {
        return saturated_fat;
    }

    public void setSaturated_fat(int saturated_fat) {
        this.saturated_fat = saturated_fat;
    }

    public double getSugars() {
        return sugars;
    }

    public void setSugars(double sugars) {
        this.sugars = sugars;
    }

    public int getSalt_100g() {
        return salt_100g;
    }

    public void setSalt_100g(int salt_100g) {
        this.salt_100g = salt_100g;
    }

    public double getProteins() {
        return proteins;
    }

    public void setProteins(double proteins) {
        this.proteins = proteins;
    }

    public double getFiber() {
        return fiber;
    }

    public void setFiber(double fiber) {
        this.fiber = fiber;
    }

    public List<String> getQualities() {
        return qualities;
    }

    public void setQualities(List<String> qualities) {
        this.qualities = qualities;
    }

    public List<String> getDefaults() {
        return defaults;
    }

    public void setDefaults(List<String> defaults) {
        this.defaults = defaults;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
